const mix = require('../src/index');

// console.log(mix);
// console.log('config get', mix.config());
// mix.config('babel', {hello: 'world'});
// console.log('config get babel', mix.config('babel'));
// mix.config('less', {hello: 'world'});
// console.log('config get less', mix.config('less'));
// mix.config('sass', {hello: 'world'});
// console.log('config get scss', mix.config('sass'));

mix.js({
	name: 'app.js',
	sourcemaps: false,
	firstExec: true,
	babel: {
		sourceType: "script"
	},
	src: './resources/js/*.js',
	watch: './resources/js/*.js',
	dist: './dist/js',
	concat: 'app.js',
	header: '/* this is app.js. */\n'
});

mix.vue({
	name: 'vue.components',
	src: './resources/js/components/*.vue',
	dist: './dist/js'
});

mix.css({
	name: 'css',
	src: './resources/css/*.css',
	dist: './dist/css',
	concat: 'app.css',
});

mix.sass({
	name: 'scss',
	src: './resources/scss/*.scss',
	dist: './dist/scss'
});

mix.less({
	name: 'less',
	src: [
		'./resources/less/*.less',
		'!./resources/less/_*.less'
	],
	watch: './resources/less/*.less',
	dist: './dist/less'
});

mix.image({
	name: 'image',
	src: './resources/images/*.{png,jpg,gif,ico}',
	dist: './dist/images'
});

mix.exec();


// 开启浏览器同步
// mix.browserSync({
// 	proxy: 'blog.test.com',
// 	watch: './resources/**/*.*'
// });
// mix.browserSync({
// 	proxy: 'http://127.0.0.1'
// });
