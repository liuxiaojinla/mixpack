const request = require('./request');

function hello() {
	console.log('hello world', this.name,);
}

function eat() {
	console.log(this.name, 'eating.')
}


function Person(name) {
	this.name = name;
}

Person.prototype.eat = eat;
Person.prototype.say = hello;


const person = new Person('小明');
person.eat();
person.say();


/** 获取网络数据 **/
request.get('http://www.baidu.com');
request.post('http://www.baidu.com');
