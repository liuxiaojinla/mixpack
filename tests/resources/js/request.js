(() => {
	console.log('execute task1...')
})();

(() => {
	console.log('execute task2...')
})();

(() => {
	console.log('execute task3...')
})();


module.exports = {
	get: function(url) {
		console.log(`get ${url} data.`);
	},
	post: function(url) {
		console.log(`post ${url} data.`);
	},
};
