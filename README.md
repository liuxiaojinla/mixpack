# xin-mixpack

#### 介绍
**xin-mixpack**
是一个基于gulp封装的轻量级前端处理插件，它让你重复而繁琐的gulp配置变得简洁，目前已支持js,css,less,sass,vue。

下个版本将基于webpack实现，目前就这样先用着吧


#### 安装教程
`npm i -g xin-mixpack`

`gulp `

#### 使用说明
    // 构建 JS
    mix.js({
        name: 'app.js',
        src: './resources/js/*.js',
        watch: './resources/js/*.js',
        dist: './dist/js',
        concat: 'app.js',
        header: '/* this is app.js. */\n'
    });

    // 构建 vue
    mix.vue({
        name: 'vue.components',
        src: './resources/js/components/*.vue',
        dist: './dist/js'
    });
    
    // 构建 css
    mix.css({
        name: 'css',
        src: './resources/css/*.css',
        dist: './dist/css',
        concat: 'app.css',
    });
    
    // 构建 sass
    mix.sass({
        name: 'scss',
        src: './resources/scss/*.scss',
        dist: './dist/scss'
    });

    // 构建 less
    mix.less({
        name: 'less',
        src: [
            './resources/less/*.less',
            '!./resources/less/_*.less'
        ],
        watch: './resources/less/*.less',
        dist: './dist/less'
    });
