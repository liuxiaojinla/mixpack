const gulp = require('gulp');
const minimist = require('minimist');

const config = require('./config/index');
const actions = require('./actions/index');

/**
 * 随机字符串
 * @param len
 * @return {string|string}
 */
function randomString(len) {
	len = len || 32;
	const $chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';
	/****默认去掉了容易混淆的字符oOLl,9gq,Vv,Uu,I1****/
	const maxPos = $chars.length;
	let pwd = '';
	for (let i = 0; i < len; i++) {
		pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
	}
	return pwd;
}

/**
 * 处理命令行
 * @type {{default: {env: string | string}, string: string}}
 */
const knownOptions = {
	string: 'env',
	default: {env: process.env.NODE_ENV || 'production'}
};
const options = minimist(process.argv.slice(2), knownOptions);
const isProduction = options.env === 'production';

const tasks = [];

// 获取要首次执行的任务
function getFirstExecuteTasks(tasks) {
	const result = [];

	tasks.forEach(task => {
		if (task.firstExec) {
			result.push(task.name);
		}
	});

	return result;
}

/**
 * 导出相关接口
 * @type {{isProduction(): *, config: ((function(*=, *=): (*|*|undefined))|*), push: module.exports.push, exec: module.exports.exec}}
 */
module.exports = {
	push: function(callback) {
		tasks.push(callback);
	},
	exec: function() {
		const series = getFirstExecuteTasks(tasks);
		series.push(function() {
			console.log('start file watching.');
			for (const task of tasks) {
				console.log(task.name, 'watching files', task.watch || task.src);
				gulp.watch(task.watch || task.src, gulp.series(task.name));
			}
		});
		gulp.task('default', gulp.series(series));
		// gulp.task('default', gulp.parallel(...tasks));
	},
	browserSync(options) {
		const browserSync = require('browser-sync').create();
		browserSync.init(options);
		const watcher = gulp.watch(options.watch);
		watcher.on('change', function(file) {
			console.log(`watching change ${file}`);
			browserSync.reload();
		});
	},
	isProduction() {
		return isProduction;
	},
	config,
};

/**
 * 相关支持的操作
 */
for (const key in actions) {
	if (!actions.hasOwnProperty(key)) continue;

	module.exports[key] = function(options) {
		options.name = `${key} - ` + (options.name || randomString(8));

		tasks.push(options);

		gulp.task(options.name, actions[key].bind(this, ...arguments));

		return this;
	};
}
