module.exports = {
	js: require('./javascript'),
	vue: require('./vue'),
	css: require('./css'),
	sass: require('./sass'),
	less: require('./less'),
	image: require('./image'),
};
