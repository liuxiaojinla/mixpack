const gulp = require('gulp');
const gulpIf = require('gulp-if');
const gulpPlumber = require('gulp-plumber');

const gulpSass = require('gulp-sass');
const gulpSourcemaps = require('gulp-sourcemaps');

const cssHandle = require('./handles/css');

console.log(`sass config info see:`, 'https://www.npmjs.com/package/gulp-clean-css', 'https://www.npmjs.com/package/gulp-sass');

/**
 * @url https://www.npmjs.com/package/gulp-clean-css
 * @url https://www.npmjs.com/package/gulp-sass
 */
module.exports = function(options) {
	const handle = cssHandle.bind(this);
	return handle(
		options,
		gulp.src(options.src)
			.pipe(gulpPlumber())
			.pipe(gulpSourcemaps.init())
			.pipe(gulpSass(this.config('sass')))
	)
		.pipe(gulpIf(
			options.sourcemaps !== false,
			gulpSourcemaps.write('.')
		))
		.pipe(gulp.dest(function(res) {
			console.log('build SASS file', res.path);
			return options.dist;
		}));
	// .pipe(gulpNotify('Build SASS Finish'));
};
