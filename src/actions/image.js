const gulp = require('gulp');
const gulpPlumber = require('gulp-plumber');
const gulpImagemin = require('gulp-imagemin');
// const gulpNotify = require("gulp-notify");

console.log(`image config info see:`, 'https://www.npmjs.com/package/gulp-imagemin');

/**
 * @url https://www.npmjs.com/package/gulp-imagemin
 */
module.exports = function(options) {
	return gulp.src(options.src)
		.pipe(gulpPlumber())
		.pipe(gulpImagemin(this.config('image'), {
			verbose: true
		}))
		.pipe(gulp.dest(function(res) {
			console.log('build Image file', res.path);
			return options.dist;
		}));
	// .pipe(gulpNotify('Build Image Finish'));
};
