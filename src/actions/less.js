const gulp = require('gulp');
const gulpIf = require('gulp-if');

const gulpPlumber = require('gulp-plumber');
const gulpLess = require('gulp-less');
const gulpSourcemaps = require('gulp-sourcemaps');

const cssHandle = require('./handles/css');

console.log(`less config info see:`, 'https://www.npmjs.com/package/gulp-clean-css', 'https://www.npmjs.com/package/gulp-less');

/**
 * @url https://www.npmjs.com/package/gulp-clean-css
 * @url https://www.npmjs.com/package/gulp-less
 */
module.exports = function(options) {
	const handle = cssHandle.bind(this);
	return handle(
		options,
		gulp.src(options.src)
			.pipe(gulpPlumber())
			.pipe(gulpSourcemaps.init())
			.pipe(gulpLess(this.config('less')))
	)
		.pipe(gulpIf(
			options.sourcemaps !== false,
			gulpSourcemaps.write('.')
		))
		.pipe(gulp.dest(function(res) {
			console.log('build LESS file', res.path);
			return options.dist;
		}));
	// .pipe(gulpNotify('Build LESS Finish'));
};
