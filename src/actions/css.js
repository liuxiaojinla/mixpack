const gulp = require('gulp');
const gulpIf = require('gulp-if');

const gulpPlumber = require('gulp-plumber');
const gulpSourcemaps = require('gulp-sourcemaps');

const cssHandle = require('./handles/css');

console.log(`css config info see:`, 'https://www.npmjs.com/package/gulp-clean-css');

/**
 * @url https://www.npmjs.com/package/gulp-clean-css
 */
module.exports = function(options) {
	const handle = cssHandle.bind(this);
	return handle(
		options,
		gulp.src(options.src)
			.pipe(gulpPlumber())
	)
		.pipe(gulpIf(
			options.sourcemaps !== false,
			gulpSourcemaps.write('.')
		))
		.pipe(gulp.dest(function(res) {
			console.log('build CSS file', res.path);
			return options.dist;
		}));
	// .pipe(gulpNotify('Build CSS Finish'));
};
