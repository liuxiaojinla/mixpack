const gulpIf = require('gulp-if');

const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cleanCSS = require('gulp-clean-css');
const header = require('gulp-header');
const footer = require('gulp-footer');
const concat = require('gulp-concat');

const gulpSourcemaps = require('gulp-sourcemaps');

module.exports = function(options, pipeline) {
	return pipeline
		.pipe(gulpIf(
			options.sourcemaps !== false,
			gulpSourcemaps.init()
		))
		.pipe(gulpIf(
			options.concat !== null && options.concat !== undefined,
			concat(options.concat || 'app.css', this.config('concat'))
		))
		.pipe(postcss([
			autoprefixer()
		]))
		.pipe(gulpIf(
			options.cleanCSS !== false,
			cleanCSS(Object.assign({debug: true}, this.config('cleanCss')), (details) => {
				console.log(`${details.name}: original size : ${details.stats.originalSize}b`,`minified size : ${details.stats.minifiedSize}`);
			})
		))
		.pipe(gulpIf(
			options.header !== null && options.header !== undefined,
			header(options.header)
		))
		.pipe(gulpIf(
			options.footer !== null && options.footer !== undefined,
			footer(options.footer)
		));
};

