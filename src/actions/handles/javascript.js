const gulpIf = require('gulp-if');

const header = require('gulp-header');
const footer = require('gulp-footer');
const babel = require('gulp-babel');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const gulpSourcemaps = require('gulp-sourcemaps');

module.exports = function(options, pipeline) {
	const babelOptions = Object.assign({}, this.config('babel') || {}, options.babel || {});
	const uglifyOptions = Object.assign({}, this.config('uglify') || {}, options.uglify || {});
	const concatOptions = Object.assign({}, this.config('concat') || {}, typeof options.concat === "object" ? (options.concat || {}) : ({
		file: options.concat || 'app.js'
	}));

	return pipeline
		.pipe(gulpIf(
			options.sourcemaps !== false,
			gulpSourcemaps.init()
		))
		.pipe(gulpIf(
			options.concat !== null && options.concat !== undefined,
			concat(concatOptions.file, concatOptions)
		))
		.pipe(gulpIf(
			options.babel !== false,
			babel(babelOptions)
		))
		.pipe(gulpIf(
			options.uglify !== false,
			uglify(uglifyOptions)
		))
		.pipe(gulpIf(
			options.header !== null && options.header !== undefined,
			header(options.header)
		))
		.pipe(gulpIf(
			options.footer !== null && options.footer !== undefined,
			footer(options.footer)
		));
};
