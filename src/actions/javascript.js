const gulp = require('gulp');
const gulpIf = require('gulp-if');

const gulpPlumber = require('gulp-plumber');
// const gulpNotify = require("gulp-notify");
const gulpSourcemaps = require('gulp-sourcemaps');

const jsHandle = require('./handles/javascript');

console.log(`javascript config info see:`, 'https://www.npmjs.com/package/gulp-babel');

/**
 * @url https://www.npmjs.com/package/gulp-babel
 */
module.exports = function(options) {
	const handle = jsHandle.bind(this);
	return handle(
		options,
		gulp.src(options.src)
			.pipe(gulpPlumber())
	)
		.pipe(gulpIf(
			options.sourcemaps !== false,
			gulpSourcemaps.write('.')
		))
		.pipe(gulp.dest(function(res) {
			console.log('build javascript file', res.path);
			return options.dist;
		}));
	// .pipe(gulpNotify('Build Javascript Finish'));
};
