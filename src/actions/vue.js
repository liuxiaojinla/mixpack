const gulp = require('gulp');
const gulpIf = require('gulp-if');

const gulpPlumber = require('gulp-plumber');
const gulpSourcemaps = require('gulp-sourcemaps');

const gulpVueComponent = require('gulp-vue-single-file-component');
const inlineVue = require('vue-template-inline');

const jsHandle = require('./handles/javascript');

console.log(`vue config info see:`, 'https://www.npmjs.com/package/gulp-babel', 'https://www.npmjs.com/package/gulp-vue-single-file-component');

/**
 * @url https://www.npmjs.com/package/gulp-babel
 * @url https://www.npmjs.com/package/gulp-vue-single-file-component
 */
module.exports = function(options) {
	const handle = jsHandle.bind(this);
	return handle(
		options,
		gulp.src(options.src)
			.pipe(gulpPlumber())
			// .pipe(gulpSourcemaps.init())
			.pipe(gulpVueComponent(this.config('vueComponent')))
			.pipe(inlineVue())
	)
		.pipe(gulpIf(
			options.sourcemaps !== false,
			gulpSourcemaps.write('.')
		))
		.pipe(gulp.dest(function(res) {
			console.log('build VUE Component file', res.path);
			return options.dist;
		}));
	// .pipe(gulpNotify('Build VUE Component Finish'));
};
