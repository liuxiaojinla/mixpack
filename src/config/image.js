const gulpImagemin = require('gulp-imagemin');
const pngquant = require('imagemin-pngquant');

module.exports = [
	gulpImagemin.gifsicle({interlaced: true}),
	gulpImagemin.jpegtran({progressive: true}),
	gulpImagemin.optipng({
		optimizationLevel: 80,
		plugins: [
			pngquant()
		]
	}),
	gulpImagemin.svgo({
		plugins: [
			{removeViewBox: true},
			{cleanupIDs: false}
		]
	})
];
