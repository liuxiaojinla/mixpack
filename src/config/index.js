const config = {
	babel: require('./babel'),
	uglify: require('./uglify'),
	vueComponent: require('./vue-component'),
	postcss: require('./postcss'),
	cleanCss: require('./clean-css'),
	less: require('./less'),
	sass: require('./sass'),
	concat: require('./concat'),
	sourcemaps: require('./sourcemaps'),
	image: require('./image'),
};

module.exports = function(name = null, value = null) {
	if (name === null) return config;

	if (typeof name === 'object') {
		Object.assign(config, name);
	}

	if (!(name in config)) {
		throw new Error(`配置${name}不存在`);
	}

	if (value === null) return config[name];

	Object.assign(config[name], value);
};
